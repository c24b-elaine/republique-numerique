#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Outils pour manipuler les datasets
"""

__author__ = 'Constance de Quatrebarbes (4barbes@gmail.com)'
__copyright__ = 'Copyright (c) 2015 Constance de Quatrebarbes'
__license__ = 'GPLv3'
__vcs_id__ = '$Id$'
__version__ = '0.0.1'

import json, csv
import re
from pandas.io.json import json_normalize
import collections
import pandas as pd

def clean_text(text):
    return re.sub("\n|\t|\s\s", "", text.strip())
    
def flatten(data):
    '''flatten list and dict from depth 1 to depth 0'''
    new_data = {}
    for key,value in data.items():
        if isinstance(value, list):
            new_dict = {}
            listes = []
            if len(value) != 0:
                listes.append([key,value[0].keys()])                
                for n in listes:
                    if len(n[1]) > 0:
                        for item in n[1]:
                            new_dict[n[0]+"_"+item] = []
                    
                             
                for k in new_dict.keys():
                    pkey, nkey = k.split("_", 1)                        
                    for item in data[pkey]:
                        try:
                            new_i = item[nkey].encode("utf-8")
                        except AttributeError:
                            new_i = str(item[nkey]).encode("utf-8")
                        new_dict[pkey+"_"+nkey].append(new_i)
            else:
                new_dict[key] = []
            
            for key, value in new_dict.items():
                if isinstance(value,list):
                    new_dict[key] = ("***").join(value)
        
            new_data.update(new_dict)
        
        elif isinstance(value, dict):
            for k,v in value.items():
                try:
                    new_data[key+"_"+k] = v.encode("utf-8")
                except AttributeError:
                    new_data[key+"_"+k] = str(v).encode("utf-8")
                except UnicodeDecodeError:
                    new_data[key+"_"+k] = unicode(value.decode("utf-8")).encode("utf-8")
        else:
            try:
                new_data[key] = value.encode("utf-8")
            except AttributeError:
                new_data[key] = str(value).encode("utf-8")
            except UnicodeDecodeError:
                new_data[key] = unicode(value.decode("utf-8")).encode("utf-8")
    return new_data
    

    
def read_from_json(jsonfile):
    with open(jsonfile, "r") as f:
        data = json.load(f)
    return data

def read_from_csv(csvfile, sep=","):
    return pd.read_csv(csvfile, sep=sep)
    
def write_to_json(data, jsonfile):
    with open(jsonfile, "w") as f:
        data = json.dumps(data, indent=4)
        f.write(unicode(data))
    return
    
def write_to_csv(data, csvfile, delimiter="\t"):
    '''
    Write in tabular mode 
    from a dict or a nested dict
    '''
    if isinstance(data, list):
        header = data[0]
        if isinstance(header, list):
            #here means it's a simple matrix
            with open(csvfile, "w") as f:
                csvwriter = csv.writer(f, delimiter=delimiter)
                for row in data:
                    csvwriter.writerow(row)
                
        elif isinstance(header, dict):
            #here means it's a simple dict:
            #so mapp the 1st set of keys as column_name
            with open(csvfile, "w") as f:
                csvwriter = csv.writer(f, delimiter=delimiter)
                csvwriter.writerow(header.keys())
                for row in data:
                    csvwriter.writerow(row.values())
                    
            
    elif isinstance(data, dict):
        #here means it's a simple dict:
        #so mapp the 1st set of keys as column_name
        with open(csvfile, "w") as f:
            csvwriter = csv.writer(f, delimiter=delimiter)
            csvwriter.writerow(data.keys())
            csvwriter.writerow(data.values())
    
    else:
        print("ERROR: Invalid format of type %s. Data can't be written to a csv file") %type(data)
    return
  
def normalize(data):
    '''
    les données en entrées sont une liste d'enregistrement en dictionnaires
    chaque dictionnaire a des clés valeurs
    ajoute les clés valeurs manquantes à certains dictionnaires
    en "aplatissant" les nours du dictionnaires
    pour permettre la vision tabulaire
    '''
    items_keys = set()
    #building the reference of columns
    for item in data:
        new_item = flatten(item)
        items_keys.update(new_item.keys())
    items_keys = [str(n).encode("utf-8") for n in list(set(items_keys))]
    #building new default dict
    #with all the keys awaited, values are set to None in this case
    new_list =[]
    for item in data:
        item_ref = collections.defaultdict.fromkeys(items_keys, u'')
        for k, v in item.items():
            try:
                item_ref[k] = v.encode("utf-8")
            except AttributeError:
                item_ref[k] = str(v).encode("utf-8")
        new_list.append(item_ref)
    return new_list
