#/usr/bin/python3
# coding: utf-8
import os, sys
import json, csv
import pandas as pd
from pandas.io.json import json_normalize
import collections


def flatten_list(data):
    '''return a dict'''
    new_data = {}
    for item in data:
        print items.keys()
        #print items.values()
        
def get_all_votes(data):
    '''recupérer les votes par utilisateurs '''
    participations = []
    users = []
    
    for n in data["participants"]:
        
        user = n.keys()[0]
        try:
            u_votes = n[user]["votes"]["votes"]
        
        
            df = json_normalize(u_votes)
            df["user"] = user
            participations.append(df)
        except KeyError:
            pass
    all_votes = pd.concat(participations)
    return all_votes

def get_all_actions_by_user(data, action_type="sources"):
    '''recupérer les propositions d'articles par utilisateurs'''
    participations = []
    for n in data["participants"]:
        user = n.keys()[0]
        try:
            u_votes = n[user][action_type][action_type]
            #print u_votes
            df = json_normalize(u_votes)
            df["user"] = user
            participations.append(df)    
        except KeyError:
            pass
        except IndexError:
            pass
    if len(participations) > 0:
        return pd.concat(participations)
    else:
        sys.exit("%s format problem" %action_type)

def flatten(data):
    '''flatten list and dict from depth 1 to depth 0'''
    new_data = {}
    for key,value in data.items():
        if isinstance(value, list):
            new_dict = {}
            listes = []
            if len(value) != 0:
                listes.append([key,value[0].keys()])                
                for n in listes:
                    if len(n[1]) > 0:
                        for item in n[1]:
                            new_dict[n[0]+"_"+item] = []
                    
                             
                for k in new_dict.keys():
                    pkey, nkey = k.split("_", 1)                        
                    for item in data[pkey]:
                        try:
                            new_i = item[nkey].encode("utf-8")
                        except AttributeError:
                            new_i = str(item[nkey]).encode("utf-8")
                        new_dict[pkey+"_"+nkey].append(new_i)
            else:
                new_dict[key] = []
            
            for key, value in new_dict.items():
                if isinstance(value,list):
                    new_dict[key] = ("***").join(value)
        
            new_data.update(new_dict)
        
        elif isinstance(value, dict):
            for k,v in value.items():
                try:
                    new_data[key+"_"+k] = v.encode("utf-8")
                except AttributeError:
                    new_data[key+"_"+k] = str(v).encode("utf-8")
                except UnicodeDecodeError:
                    new_data[key+"_"+k] = unicode(value.decode("utf-8")).encode("utf-8")
        else:
            try:
                new_data[key] = value.encode("utf-8")
            except AttributeError:
                new_data[key] = str(value).encode("utf-8")
            except UnicodeDecodeError:
                new_data[key] = unicode(value.decode("utf-8")).encode("utf-8")
    return new_data
    

    
def read_from_json(jsonfile):
    with open(jsonfile, "r") as f:
        data = json.load(f)
    return data

def read_from_csv(csvfile):
    with open(csvfile, "r") as f:
        reader = csv.reader(f, delimiter="\t")
    return reader

def write_to_json(data, jsonfile):
    with open(jsonfile, "w") as f:
        data = json.dumps(data, indent=4)
        f.write(unicode(data))
    return
    
def write_to_csv(data, csvfile, delimiter="\t"):
    '''
    Write in tabular mode 
    from a dict or a nested dict or a dataframe
    '''
    
    if isinstance(data, list):
        header = data[0]
        if isinstance(header, list):
            #here means it's a simple matrix
            with open(csvfile, "w") as f:
                csvwriter = csv.writer(f, delimiter=delimiter)
                for row in data:
                    csvwriter.writerow(row)
                
        elif isinstance(header, dict):
            #here means it's a simple dict:
            #so mapp the 1st set of keys as column_name
            with open(csvfile, "w") as f:
                csvwriter = csv.writer(f, delimiter=delimiter)
                csvwriter.writerow(header.keys())
                for row in data:
                    csvwriter.writerow(row.values())
                    
    
        
    elif isinstance(data, dict):
        #here means it's a simple dict:
        #so mapp the 1st set of keys as column_name
        with open(csvfile, "w") as f:
            csvwriter = csv.writer(f, delimiter=delimiter)
            csvwriter.writerow(data.keys())
            csvwriter.writerow(data.values())
    
    else:
        data.to_csv(path_or_buf=csvfile, sep=delimiter, mode='w', encoding="utf-8")
        #print("ERROR: Invalid format of type %s. Data can't be written to a csv file") %type(data)
    return
  
def normalize(data):
    '''
    les données en entrées sont une liste d'enregistrement en dictionnaires
    chaque dictionnaire a des clés valeurs
    ajoute les clés valeurs manquantes à certains dictionnaires
    en "aplatissant" les nours du dictionnaires
    pour permettre la vision tabulaire
    '''
    items_keys = set()
    #building the reference of columns
    for item in data:
        new_item = flatten(item)
        items_keys.update(new_item.keys())
    items_keys = [str(n).encode("utf-8") for n in list(set(items_keys))]
    #building new default dict
    #with all the keys awaited, values are set to None in this case
    new_list =[]
    for item in data:
        item_ref = collections.defaultdict.fromkeys(items_keys, u'')
        for k, v in item.items():
            try:
                item_ref[k] = v.encode("utf-8")
            except AttributeError:
                item_ref[k] = str(v).encode("utf-8")
            except UnicodeDecodeError:
                item_ref[k] = str(v)
        new_list.append(item_ref)
    return new_list
    

            
if __name__ == "__main__":
    #les différents dossiers où sont stockés les fichiers
    current_path = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
    dataset_dir = os.path.join(current_path, "datasets") 
    user_dir = os.path.join(dataset_dir, "participants")
    #pour tous les users json présents dans le dossier articles
    #1. générer un fichier csv equivalent au fichier json avec le même nom
    #~ articles = []
    #~ for jsonf in os.listdir(user_dir):
        #~ print jsonf
        #~ if jsonf.endswith(".json"):
            #~ jsonfile = os.path.join(user_dir, jsonf)
            #~ id_file = jsonf.split(".")[-2]
            #~ csvname = id_file+".csv"
            #~ csvfile = os.path.join(user_dir, csvname)
            #~ data = read_from_json(jsonfile)
            #~ dict_data = {}
            #~ name = data.keys()[0]
            #~ dict_data["id"] = name
            #~ for k, v in data[name].items():
                #~ 
                #~ for k2,v2 in flatten(v).items():
                    #~ dict_data[k+"_"+k2] = v2
            #~ write_to_csv(dict_data, csvname)
           # articles.append(dict_data)
    #2 générer une liste agloméré d'utilisateur
    #articles_path2 = os.path.join(dataset_dir, "participants_from_details.csv")
    #write_to_csv(normalize(articles), articles_path2)
    
    
    #3. générer un fichier csv a partir d'un fichier json particpants.json
    #~ users = os.path.join(dataset_dir, "participants.json")
    #~ users_out = os.path.join(dataset_dir, "details_participants.csv")
    #~ d = read_from_json(users)
    #~ full_data = []
    #~ for data in d["participants"]:
        #~ dict_data = {}
        #~ name = data.keys()[0]
        #~ dict_data["id"] = name
        #~ for k, v in data[name].items():
            #~ for k2,v2 in flatten(v).items():
                #~ dict_data[k+"_"+k2] = v2
        #~ full_data.append(dict_data)
    #~ full_data = normalize(full_data)
    #~ 
    #~ write_to_csv(full_data, users_out)
    
    
    #4. Fichier à plat de tous les votes par utilisateurs
    users = os.path.join(dataset_dir, "participants.json")
    votes= os.path.join(dataset_dir, "votes.csv")
    data = read_from_json(users)
    write_to_csv(get_all_actions_by_user(data, "votes"), votes)
    #5.fichier à plat de toutes les modifications par utilisateurs
    votes= os.path.join(dataset_dir, "propositions.csv")
    data = read_from_json(users)
    write_to_csv(get_all_actions_by_user(data, "propositions"), votes)
    #6.fichier à plat de toutes les sources par utilisateurs
    votes= os.path.join(dataset_dir, "sources.csv")
    data = read_from_json(users)
    write_to_csv(get_all_actions_by_user(data, "sources"), votes)
    #7.fichier à plat de tous les arguments par utilisateurs
    votes= os.path.join(dataset_dir, "arguments.csv")
    data = read_from_json(users)
    write_to_csv(get_all_actions_by_user(data, "arguments"), votes)
