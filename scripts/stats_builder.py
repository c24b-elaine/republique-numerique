#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Modeste fumisterie
"""

__author__ = 'François Régnier (frregnier@gmail.com)'
__copyright__ = 'Copyright (c) 2015 François Régnier'
__license__ = 'GPLv3'
__vcs_id__ = '$Id$'
__version__ = '0.0.1'


import logging as log
import sys
import ConfigParser, os
import argparse
import inspect
import traceback
import json

def gc(func):
    def habille(*args, **kwargs):
        ret = None
        try:
            ret = func(*args, **kwargs)
        except Exception, e:
            try:
                log.error("ramasse_miettes fc:(%r): %r"%(func.__name__, e))
            except Exception, e:
                print "ramasse_miettes broken %r"%e
                [log] and log.error("ramasse_miettes broken %r"%e)
        return ret
    return habille

def tbk(func):
    def habille(*args, **kwargs):
        ret = None
        try:
            ret = func(*args, **kwargs)
        except Exception, e:
            try:
                log.error("tracebk fc:(%r): %r"%(func.__name__, e))
                log.error("tracebk excinfo: %r %r %r"%sys.exc_info())
                #lines = traceback.format_exc()
                exc_type, exc_value, exc_traceback = sys.exc_info()
                #for line in lines.split():
                #    log.error("tracebk line:%r"%line)
                log.error("%r"%repr(traceback.format_exception(exc_type, exc_value,
                                                          exc_traceback)))
                log.error("%r"%repr(traceback.extract_tb(exc_traceback)))
                log.error("%r"%repr(traceback.format_tb(exc_traceback)))
            except Exception, e:
                print "tracebk broken %r"%e
                [log] and log.error("tracebk broken %r"%e)
        return ret
    return habille

@tbk
def recur_keys(mon_json, lplate=[], dico={}):
    """
    parcours recursif a la recherche des noms de champs
    retourne une liste à deux elements
    1) une liste a plat des champs
    2) un dico de dico des champs (structure)
    """
    eln = 0
    if isinstance(mon_json, list):
        for el in mon_json:
            if isinstance(el, list):
                recur_keys(el, lplate, dico)
            elif isinstance(el, dict):
                for cn,vn in el.items():
                    if not cn in lplate:
                        lplate.append(cn)
                    if not cn in dico.keys():
                        dico[cn] = {}
                    recur_keys([vn], lplate, dico[cn])
    if isinstance(mon_json, dict):
        for cn,vn in mon_json.items():
            if not cn in lplate:
                lplate.append(cn)
            if not cn in dico.keys():
                dico[cn] = {}
            recur_keys([vn], lplate, dico[cn])


format_u={
    'nb_arguments': 0, 'arguments': [],
    'nb_votes': 0, 'opinion': [],
}
@gc
def cpt_arguments(jsn, users={}, min_users={}):
    j1 = json.load(open(jsn))
    for arg in j1:
        for argu in arg['argument_users']:
            try:
                if not argu['user_id'] in users.keys():
                    users[argu['user_id']] = {
                        'nb_arguments': 0, 'arguments': [],
                        'nb_votes': 0, 'opinion': [],
                    }
                    min_users[argu['user_id']] = {'nb_arguments': 0, 'nb_votes': 0}
                min_users[argu['user_id']]['nb_arguments'] += 1
                users[argu['user_id']]['nb_arguments'] += 1
                users[argu['user_id']]['arguments'].append((arg['titre'], j1.index(arg)))
            except Exception, e:
                print 'paff cpt_args: %r'%e

@gc
def cpt_opinion(jsn, users={}, min_users={}):
    j1 = json.load(open(jsn))
    for arg_n,data in j1.items():
        try:
            for vote in data['opinion']['votes']:
                try:
                    uvote = vote['user']['uniqueId']
                    if not uvote in users.keys():
                        users[uvote] = {
                            'nb_arguments': 0, 'arguments': [],
                            'nb_votes': 0, 'opinion': [],
                        }
                        min_users[uvote] = {'nb_arguments': 0, 'nb_votes': 0}
                    min_users[uvote]['nb_votes'] += 1
                    users[uvote]['nb_votes'] += 1
                    users[uvote]['opinion'].append((data['opinion']['title'], data['opinion']['id']))
                except Exception, e:
                    print 'paff cpt_args: %r'%e
        except:
            pass

if __name__ == '__main__':
    users={}
    min_users={}
    cpt_arguments('scripts/extraction/data/votes.json', users,min_users)
    open('tools.arguments.votes.json','w').write(json.dumps(users))
    cpt_opinion('arguments_all.json', users,min_users)
    open('tools.arguments_partcipe.opinion_vote.json','w').write(json.dumps(users))
    open('tools.users.stats.json','w').write(json.dumps(min_users))
    
