#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Outils pour manipuler l'API
"""

__author__ = 'Constance de Quatrebarbes (4barbes@gmail.com)'
__copyright__ = 'Copyright (c) 2015 Constance de Quatrebarbes'
__license__ = 'GPLv3'
__vcs_id__ = '$Id$'
__version__ = '0.0.1'

import os
import requests
requests.adapters.DEFAULT_RETRIES = 5

from bs4 import BeautifulSoup as bs

from datasets_tool import *
API_URL = "https://www.republique-numerique.fr/api/opinions/"
current_path = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
dataset_dir = os.path.join(current_path, "datasets")
articlef_dir = os.path.join(dataset_dir, "articles_full")
opinion_dir = os.path.join(dataset_dir, "opinions")
profile_dir = os.path.join(dataset_dir, "participants")
sources_dir = os.path.join(dataset_dir, "sources")
arguments_dir = os.path.join(dataset_dir, "arguments")
votes_dir = os.path.join(dataset_dir, "votes")
propositions_dir = os.path.join(dataset_dir, "propositions")
answer_dir = os.path.join(dataset_dir, "reponses")
historique = os.path.join(dataset_dir, "historique")

def extract():
    '''le point d'API est fonction de l'id_opinion(oid) compris entre 60 et 782 
    soit 722 articles
    tous n'étant pas dispo on retrouve les 696 articles annoncés
    '''
    urls = []
    articles = []
    #versions agregées de l'article
    for oid in xrange(61,783):
        
        article_url = API_URL+str(oid)
        urls.append([oid, article_url])
        r = requests.get(article_url)
        
        try:
            raw_data = r.json()["opinion"]
            
            raw_f = os.path.join(opinion_dir, str(oid)+".json")
            #ecrire le fichier article correspondant en mode brut
            write_to_json(raw_data, raw_f)
            a = extract_article(oid, raw_data)
            a["arguments"] = extract_arguments(oid, raw_data["arguments"])
            a["sources"] = extract_sources(oid, raw_data["sources"])
            a["versions"] = extract_amendements(oid)
            a["answer"] = extract_answer(oid, raw_data["answer"])
            
            articles.append(a)
        except KeyError:
            pass
    raw_f = os.path.join(dataset_dir, "articles_full.json")
    write_to_json({"articles":articles}, raw_f)
    return articles
    
def extract_article(oid, data):
    '''extraire le contenu des articles et leur id unique, nombre de votes, nombre d'arguments, '''
    article = {"article_id":oid}
    for k in ["votes_total", "created_at", "updated_at", "body", "versions_count", "sources_count", "arguments_count", "ranking", "title", "votes_nok", "votes_ok", "votes_mitige"]:
        article[k] = data[k]
    article["body"] = bs(article["body"])
    article["body_links"] = [n.get("href") for n in article["body"].find_all("a")]
    article["body_anchors"] = [n.text for n in article["body"].find_all("a")]
    article["body"] = clean_text(article["body"].text)
    article["author"]= data["author"]["uniqueId"]
    article["article_link"] = data["_links"]["show"].replace("\/", "/")
    article["subtitle"] = bs(data["type"]["subtitle"]).text
    article['title'] = bs(data["type"]['title']).text
    article["cat_id"] = data["type"]['id']
    return article
    

    
    
def extract_amendements(oid):
    '''extraire les propositions de modifications correspond aux propositions --> versions
    '''
    vurl = API_URL+str(oid)+"/versions"
    
    r = requests.get(vurl)
    data = r.json()
    #print data.keys()
    raw_data = r.json()
    try:
        versions = []
        for n in raw_data["versions"]:
            proposition = {"article_id": oid}
            
            for k,v in n.items():
                if k == "title":
                    proposition[k] = bs(v).text
                elif k in [u'created_at', u'updated_at', u'slug', u'id']:
                    proposition[k] = v
                if k in [u'sources', u'arguments_count', u'votes_total', u'votes', u'arguments', u'votes_mitige', u'arguments_yes_count', u'arguments_no_count', u'votes_nok']:
                    proposition[k] = v
                else:
                    pass
                proposition["link"] = (n['_links']['show']).replace("\/", "/")
            proposition["author"] = n["author"]["uniqueId"]
            
            details = vurl+"/"+str(n["id"])
            r = requests.get(details)
            detail = r.json()["version"]
            
            try:
                change = detail["parent"]["modals"][0]
                
                proposition["after"] = clean_text(bs(change["after"]).text)
                proposition["before"] = clean_text(bs(change["before"]).text)
                proposition["title"] = clean_text(bs(change["title"]).text)
            except IndexError:
                proposition["after"] = None
                proposition["before"] = None
                proposition["title"] = None
            if detail["comment"] is not None:
                proposition["comment"] = bs(detail["comment"]).text
            else:
                proposition["comment"]= None
                
            versions.append(proposition)
        return versions
    except KeyError:
        return versions
    

def extract_answer(oid, answer):
    '''extraire la réponse du gouvernement'''
    if answer is None:
        answer_f = {}
        return answer_f
    try:
        answer_f = {"article_id":oid}
        #answer_f["answer_title"] = bs(answer["title"]).text
        answer_f["body"] = clean_text((bs(answer["body"]).text).encode("utf-8"))
        links = [n.get("href") for n in bs(answer["body"]).find_all("a")]
        answer_f["answer_refs"] = [n for n in links if not n.startswith("/profile/")]
        answer_f["cited_users"] = [(n.replace("/profile/", "")).encode("utf-8") for n in links if n.startswith("/profile/")]
        answer_f["author"] = answer["author"]["uniqueId"]
        
    except TypeError:
        answer_f = {}
    return answer_f
    
def extract_arguments(oid, data):
    '''extraire les arguments et leur votes'''
    arguments = []
    for ar in data:
        arg = {"article_id":oid} 
        for k, v in ar.items():
            if k in [u'body', u'author', u'created_at', u'updated_at', u'votes_count', u'id']:
                if k == "body":
                    arg[k] = clean_text(bs(v).text).encode("utf-8")
                else:
                    arg[k] = str(v).encode("utf-8")
        
        arg["author"]= str(ar["author"]["uniqueId"]).encode('utf-8')
        arg["link"] = ar["_links"]["show"].replace("\/", "/")
        arg["link"] = arg["link"]+"#arg-"+arg["id"]
        arguments.append(arg)
    return arguments
    
def extract_sources(oid, data):
    '''extraire les arguments et leur votes'''
    #print data
    arguments = []
    for s in data:
        arg = {"article_id":oid} 
        for k,v in s.items():
            if k in [u'body', u'category', u'title', u'created_at', u'updated_at', u'link',u'votes_count', u'id']:
                if k in ["body", "category", "title"]:
                    arg[k] = clean_text(bs(v).text).encode("utf-8")
                else:
                    arg[k] = str(v).encode("utf-8")
        arg["source_link"] = arg["link"]
        arg["link"] = arg["link"]+"#source-"+arg["id"]
        arg["author"]= str(s["author"]["uniqueId"]).encode("utf-8")
        arguments.append(arg)
    return arguments
    
    
def extract_votes():
    '''extraire tous les votes de chaque participant
    seuls les contributeurs ayant amendé, sourcé ou argumenté sont mentionnés
    et non l'integralités des votants dans l'api
    on extrait donc a partir du site
    '''
    BASE_URL = "https://www.republique-numerique.fr/projects/projet-de-loi-numerique"
    participants = {"participants": []}
    opinion = {"label-danger":-1,"label-warning": 0, "label-success": 1} 
    errors = []
    
    for x in range(1,1335):
        members_url = BASE_URL+"/participants/"+str(x)
        r = requests.get(members_url)
        if r.status_code == 200:
            
            tree = bs(r.text)
            for n in tree.find_all("div",{"class":"media-body"}):
                
                user_link = n.find("a").get("href")
                username = user_link.replace("/profile/","")
                participant = {"username":username, "user_profile": user_link, "votes":[]}
                try:
                    stats = re.sub("\n|\t|\r", "", n.find("span",{"class":"excerpt"}).text).encode("utf-8")
                    stats = re.sub("\s\s\s", "", stats).strip().split(" ")
                    votes = int(stats[4])
                    print username
                    if votes > 0:
                        
                        r = requests.get(user_link+"/votes")
                        tree = bs(r.text)
                        votes_list = tree.find("ul", {"class":"media-list"})
                        votes_list = votes_list.find_all("div",{"class":"opinion__data"})
                        votes_ids = votes_list.find_all("li",{"class":"opinion"})
                        votes_ids = [n.get("id") for n in votes_ids]
                        
                        print username, votes, len(votes_list)
                        
                        if len(votes_list) > 0:
                        
                            for n in votes_list:
                                vote = {}
                                article = n.find_all("a")[-1]
                                vote["article_link"] = article.get("href")
                                vote["article_title"] = clean_text(article.text).encode("utf-8")
                                vote["date"] = (n.find("p", {"class":"opinion__date"}).text).encode("utf-8")
                                vote["vote"] = opinion[n.find_all("span", {"class":"label"})[0].get("class")[-1]]
                                participant["votes"].append(vote)
                    
                        
                except AttributeError:
                    pass
                raw_f = os.path.join(profile_dir, username+".json")
                if os.path.exists(raw_f):
                   pass
                else:
                    write_to_json(participant, raw_f)
                
                participants["participants"].append(participants)
        else:
            errors.append(members_url)
    write_to_json(errors, "extract_missing.csv")
    return participants
    
if __name__=="__main__":
    
    #génerer la liste des articles
    extract()
    #générer la liste des participants
    #extract_votes()
    #genérer un seul fichier de modifications
    #extract_amendements(61)
