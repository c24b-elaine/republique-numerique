#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Outils pour manipuler les datasets
"""

__author__ = 'Constance de Quatrebarbes (4barbes@gmail.com)'
__copyright__ = 'Copyright (c) 2015 Constance de Quatrebarbes'
__license__ = 'GPLv3'
__vcs_id__ = '$Id$'
__version__ = '0.0.1'

import os
import json, csv
import re
from pandas.io.json import json_normalize
import collections
import pandas as pd
from tools import *
from bs4 import BeautifulSoup as bs
current_path = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
dataset_dir = os.path.join(current_path, "datasets")

opinion_dir = os.path.join(dataset_dir, "opinions")
profile_dir = os.path.join(dataset_dir, "participants")
sources_dir = os.path.join(dataset_dir, "sources")
arguments_dir = os.path.join(dataset_dir, "arguments")
votes_dir = os.path.join(dataset_dir, "votes")
propositions_dir = os.path.join(dataset_dir, "propositions")
historique = os.path.join(dataset_dir, "historique")

articlesf = os.path.join(dataset_dir, "articles.json")
    
def article_stats():
    '''Vue simplifie de l'ensemble des articles'''
    # a partir du fichier des articles
    data = read_from_json(articlesf)
    articles_view = os.path.join(dataset_dir, "stats_articles.csv")
    header = [u'_id', u'cat_id', u'updated_at', u'arguments_count', u'versions_count', u'votes_total', u'sources_count', u'votes_ok', u'created_at', u'votes_mitige', u'_id', u'votes_nok']
    new_data = []
    for art in data["articles"]:
        art["article_link"] = art["link"]
        art["article_id"] = art["_id"]
        for k in (set(art.keys()) - set(header)):
            del art[k]
        
        
        new_data.append(art)
    return write_to_csv(new_data, articles_view)

def article_info():
    '''Vue simplifie de l'ensemble des articles'''
    # a partir du fichier des articles
    data = read_from_json(articlesf)
    articles_view = os.path.join(dataset_dir, "info_articles.csv")
    header = ["title", "subtitle", "body", "author"]
    new_data = []
    for art in data["articles"]:
        art["article_id"] = art["_id"]
        art["article_link"] = art["link"]
        del art["_id"]
        del art["link"]
        for k in  (set(art.keys()) - set(header)):
            del art[k]
            
        for k,v in art.items():
            try:
                art[k] = str(v).encode("utf-8")
            except:
                art[k] = clean_text((bs(v).text).encode("utf-8"))
            
        new_data.append(art)
    return write_to_csv(new_data, articles_view)
    
def arguments_view():
    '''Vue simplifie de l'ensemble des arguments'''
    articles_view = os.path.join(dataset_dir, "arguments.csv")
    data = read_from_json(articlesf)
    arguments = []
    for art in data["articles"]:
        _id = art["_id"]
        url = art["link"]
        for arg in art["arguments"]:
            arg["article_id"] = _id
            arg["article_link"] = url
            for k,v in arg.items():
                try:
                    arg[k] = str(v).encode("utf-8")
                except:
                    arg[k] = clean_text((bs(v).text).encode("utf-8"))
            arguments.append(arg)
    return write_to_csv(arguments, articles_view)
    
def votes_view():
    '''Vue simplifiée des votes'''
    votes = []
    articles_view = os.path.join(dataset_dir, "votes.csv")
    for f in os.listdir(profile_dir):
        f = os.path.join(profile_dir, f)
        user = read_from_json(f)
        #print user["username"]
        for n in user["votes"]:
            n["vote"] = str(n["vote"]).encode("utf-8")
            n["article_title"] = (bs(n["article_title"]).text).encode("utf-8")
            n["date"] = (bs(n["date"]).text).encode("utf-8")
            n["username"] = user["username"]
            
            n["user_profile"] = user["user_profile"]
            link = n["article_link"]
            
            
            if "#arg" in link:
                n["vote_type"] = "argument"
            elif "#source" in n["article_link"]:
                n["vote_type"] = "sources"
            elif "/versions/" in link:
                n["vote_type"] = "modification"
            else:
                n["vote_type"] = "article"
            votes.append(n)
    return write_to_csv(votes, articles_view)

def participants_stats():
    '''Vue simplifiée des votes'''
    votes = []
    articles_view = os.path.join(dataset_dir, "votes_stats.csv")
    for f in os.listdir(profile_dir):
        f = os.path.join(profile_dir, f)
        user = read_from_json(f)
        links = [n["article_link"] for n in user["votes"]]
        
        user["modifications_votes_nb"] = len([n for n in links if "/versions/" in n])
        user["sources_votes_nb"] = len([n for n in links if "#source" in n])
        
        user["arguments_votes_nb"] =  len([n for n in links if "#arg" in n])
        not_article = user["arguments_votes_nb"]+ user["sources_votes_nb"]+ user["modifications_votes_nb"]
        print len(links), not_article
        user["articles_votes_nb"] = len(links) - not_article
        user["total_votes_nb"] = len(links)
        del user["votes"]
        votes.append(user)
    return write_to_csv(votes, articles_view)


    

def referentiel_participants():
    from datetime import datetime
    articles_view = os.path.join(dataset_dir, "referentiel_participants.csv")
    votes = []
    for f in os.listdir(profile_dir):
        f = os.path.join(profile_dir, f)
        user = read_from_json(f)
        
        links = [n["article_link"] for n in user["votes"]]
        
        user["modifications_votes_nb"] = len([n for n in links if "/versions/" in n])
        user["sources_votes_nb"] = len([n for n in links if "#source" in n])
        
        user["arguments_votes_nb"] =  len([n for n in links if "#arg" in n])
        not_article = user["arguments_votes_nb"]+ user["sources_votes_nb"]+ user["modifications_votes_nb"]
        print len(links), not_article
        user["articles_votes_nb"] = len(links) - not_article
        user["total_votes_nb"] = len(links)
        del user["votes"]
        votes.append(user)
    return write_to_csv(votes, articles_view)

def define_action(parent, article, action_type):
    
    a = {}
    a["article_id"] = parent["_id"]
    try:
        a["date"] = article[u'created_at'] 
    except KeyError:
        a["date"] = parent[u'created_at'] 
    try:
        a["username"] = article["author"]
    except KeyError:
        a["username"] = None
    a["type"] = action_type
    
    
    if action_type !="source":
        try:
            a["article_link"] = article["link"]
        except KeyError:
            try:
                a["article_link"] = article["article_link"]
            except KeyError:
                a["article_link"] = parent["link"]
    else:
        try:
            a["article_link"] = parent["link"]
        except KeyError:
            a["article_link"] = article["link"]
    
    return a

def format_participant_votes(parent, data, action_type):
    a = {}
    a["article_id"] = None
    a["date"] = data["date"]
    try:
        a["username"] = parent["username"]
    except KeyError:
        a["username"] = None
    a["type"] = action_type
    a["article_link"] = "http://www.republique-numerique.fr"+data["article_link"]
        
    return a
    
def historique_votes():
    '''details des actions de chaque participants'''
    from datetime import datetime
    articles_view = os.path.join(dataset_dir, "historique_votes.csv")
    votes = []
    for f in os.listdir(profile_dir):
        f = os.path.join(profile_dir, f)
        user = read_from_json(f)
        
        for n in user["votes"]:
            
            #~ action["username"] = user["username"]
            #~ action["article_link"] = n["article_link"]
            
            #~ action["article_id"] = None
            action = {}
            if "#source" in n["article_link"]:
                action["type"] = "source"
            elif "/versions/" in n["article_link"]:
                action["type"] = "modification"
            elif "#arg" in  n["article_link"]:
                action["type"] = "argument"
            elif "/blog/" in n["article_link"]:
                action["type"] = "echange"
            else:
                action["type"] = "vote"
            
            if "septembre" in n["date"]:
                n["date"] = n["date"].replace("septembre", "09")
                
                
            elif "octobre" in n["date"]:
                n["date"] = n["date"].replace("octobre", "10")
            else:
                pass
                
            n["date"] = datetime.strptime(n["date"], "%d %m %Y %H:%M")
            votes.append(format_participant_votes(user, n, action["type"]))
    return votes

def actions_stats():
    ''' recupérer les dates de toutes les actions'''
    data = read_from_json(articlesf)
    
    articles = []
    
    for article in data:
        #print article
        a = {}
        a["article_id"] = article["_id"]
        a["article_link"] = article["link"]
        a["date"] = article[u'created_at']
        a["username"] = article["author"]
        #~ a["sources"] = []
        
        
        a = define_action(article, a, "article")
        articles.append(a)
        for n in article['sources']:
            
            
            a = define_action(article, n, "source")
            articles.append(a)
        for n in article["arguments"]:
            a = define_action(article, n, "argument")
            articles.append(a)
        
        for n in article["versions"]:
            a = define_action(article, n, "modification")
            articles.append(a)
    
    votes = historique_votes()    
    articles.extend(votes)
    
    f = os.path.join(historique, "historique.csv")
    write_to_csv(articles, f)    

def get_article(aid=61, tformat="csv"):
    '''methode creer une vue pour un seul article'''
    articlesf = os.path.join(dataset_dir, "articles.json")
    data = read_from_json(articlesf)["articles"]
    for n in data:
        if n["article_id"] == aid:
            if tformat == "csv":
                article = flatten(n) 
                articlefile = os.path.join(dataset_dir, "article_"+str(aid)+".csv")
                write_to_csv(article, articlefile)
                return
            else:
                
                articlefile = os.path.join(dataset_dir, "article_"+str(aid)+".json")
                write_to_csv(n, articlefile)
                return
def get_articles():
    '''methode pour recupérer tous les articles sous format csv'''
    articlesf = os.path.join(dataset_dir, "articles.json")
    data = read_from_json(articlesf)["articles"]
    articles = []
    for n in data:
        article = flatten(n) 
        article.append(article)
    articlefile = os.path.join(dataset_dir, "articles.csv")
    write_to_csv(n, articlefile)
    return
    

if __name__=="__main__":
    #~ article_stats()
    #~ article_info()
    #~ arguments_view()
    #votes_view()
    #participants_stats()
    #actions_stats()
    #referentiel_participants()
    #historique_votes()
    #get_article()
