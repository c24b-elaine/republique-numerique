
arts=json.load(open('articles_VFF.json'))
votes=json.load(open('full_participants_VF.json'))

for art in arts['articles']:
    lvote=[]
    artlink=None
    for vote in art['votes']:
        if not artlink:
            artlink=vote['link']
        elif not artlink == vote['link']:
            print("Error link mismatch: article:%s vote:%s"%(artlink, vote['link']))
        if not vote['username'] in lvote:
            lvote.append(vote['username'])
        else:
            print ("vote duplique: %s sur %s"%(vote['username'], artlink))
    my_arts[artlink]={'title': art['title'], '_id': art['_id'], 'votants':[]}
    for v in lvote:
        my_arts[artlink]['votants'].append(v)

open('my_arts.json', 'w').write(json.dumps(my_arts))


for gars in votes['participants']:
    gg=gars['username']
    vg=gars['votes']
    for vote in vg:
        link="http://www.republique-numerique.fr%s"%vote["article_link"]
        if link in my_arts:
            if not link in match_art:
                match_art[link]={'votants_ok':[], 'votants_part':[]}
            if gg in my_arts[link]['votants']:
                match_art[link]['votants_ok'].append(gg)
            else:
                match_art[link]['votants_part'].append(gg)
        nomatch_art[link]={}

open("notmatch_artvot.json","w").write(json.dumps(nomatch_art))
open("match_artvot.json","w").write(json.dumps(match_art))
