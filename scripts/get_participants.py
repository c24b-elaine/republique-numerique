#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Outils pour constituer le dataset des participations
"""

__author__ = 'Constance de Quatrebarbes (4barbes@gmail.com)'
__copyright__ = 'Copyright (c) 2015 Constance de Quatrebarbes'
__license__ = 'GPLv3'
__vcs_id__ = '$Id$'
__version__ = '0.0.1'
__doc__ ='''

Un participant:
* a voté sur x articles
* a fait x modifications sur x articles
* a proposé x articles
* a proposé x sources pour x articles
* a émis x arguments pour x articles
'''
from datetime import datetime
from bs4 import BeautifulSoup
import requests
import re
import io, json, csv
import time, datetime
import collections
import sys
from tools import *
import os
current_path = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
dataset_dir = os.path.join(current_path, "datasets")
u_dir = os.path.join(dataset_dir, "users")
udata = os.path.join(dataset_dir, "referentiel_participants.csv")
parts = os.path.join(dataset_dir, "participants.json")
URL = "http://www.republique-numerique.fr"
def clean_text(text):
    return re.sub("\n|\t|\s\s", "", text.strip())
def format_date(date):
    if "septembre" in date:
        date = date.replace("septembre", "09")
    elif "octobre" in date:
        date = date.replace("octobre", "10")
    else:
        pass
    date = datetime.datetime.strptime(date, "%d %m %Y %H:%M")
    date = datetime.datetime.strftime(date, "%Y-%m-%dT%H:%M:00+0200")
    return date
    
def define_modifications(liste):
    '''' listes modifications par le participant
    [
    {   
        
    },
    ]
    '''
    modifications = []
    for n in liste.find_all("li"):
        modification = {}
        modification["id"] = n.get("data-pie-id")
        modification["votes"] = {i:n.get(i) for i in ["data-mitige", "data-nok","data-ok"]}
        article = n.find_all("a")[-1]
        modification["link"] = unicode((article.get("href")).encode("utf-8"))
        modification["title"] = unicode(clean_text(article.text)).encode("utf-8")
        stats = n.find("p", {"class":"opinion__votes"})
        stats = [(n.getText()).split(" ") for n in stats.find_all("span") if n.get("class") is None]
        modification["stats"] = {v[1]: v[0] for v in stats}
        modifications.append(modification)
    return modifications
    
def define_sources(liste):
    '''' liste de sources pour un participant
    [
    {   
        "id": source-13094
        "source_link": #lien relatif à la source
        "source_title": #titre de la source
        "category": #type de source
        "texte": #description
        "votes": signalé comme pertinent
    },
    ]
    '''
    sources = []
    for n in liste.find_all("li"):
        source = {}
        source["id"] = n.get("id").replace("source-", "")
        source["category"] = unicode(n.find("span", {"class": "label-info"}).text).encode("utf-8")
        source["title"] = unicode(n.find("a").text).encode("utf-8")
        source["source_link"] = unicode(n.find("a").get("href")).encode("utf-8")
        source["body"] = unicode(clean_text(n.find("p",{"class": "excerpt"}).text)).encode("utf-8")
        source["votes"] = int(n.find("span",{"class":"nb-votes"}).text)
        sources.append(source)
    return sources
def define_votes(liste):
    '''' liste de votes pour un participant
    [
    {   
        "id": vote-13094
        "article_link": #lien relatif de l'article
        "article_title": #titre partiel de l'article
        "date": #format 'string' type %d %B %YYYY %H:%M
        "opinion": # -1 contre, 0 neutre, 1 pour
    },
    ]
    '''
    votes = []
    opinion = {"label-danger":-1,"label-warning": 0, "label-success": 1} 
    for n in liste.find_all("li"):
        vote = {}
        vote["id"] = str(n.get("id")).replace("vote-", "")
        article = n.find_all("a")[-1]
        vote["link"] = str(URL+article.get("href"))
        vote["article_title"] =unicode(clean_text(article.text)).encode("utf-8")
        
        vote["date"] = format_date(unicode((n.find("p", {"class":"opinion__date"}).text).encode("utf-8")))
        
        vote["opinion"] = unicode(opinion[n.find_all("span", {"class":"label"})[0].get("class")[-1]])
        votes.append(vote)
    return votes

def define_propositions(liste):
    ''' liste des modifications sur un article par participants
    appelées versions
    [{   
        "id:#id de version? ou id de l'article
        "article_link": # lein relatif à l'article
        "article_title": #titre partiel de l'article
        "stats": # -1 contre, 0 neutre, 1 pour
    },
    '''
    modifications = []
    for n in liste.find_all("li"):
        modification = {}
        modification["id"] = n.get("data-pie-id")
        modification["votes"] = {i:n.get(i) for i in ["data-mitige", "data-nok","data-ok"]}
        article = n.find_all("a")[-1]
        modification["article_link"] = unicode((article.get("href")).encode("utf-8"))
        modification["article_title"] = unicode(clean_text(article.text)).encode("utf-8")
        stats = n.find("p", {"class":"opinion__votes"})
        stats = [(n.getText()).split(" ") for n in stats.find_all("span") if n.get("class") is None]
        modification["stats"] = {v[1]: v[0] for v in stats}
        modifications.append(modification)
    return modifications
    
    
def define_arguments(liste):
    '''liste d'arguments pour un participant
    [
    {   
        "id": arg-1234
        "article_link": #
        "article_title": #titre partiel de l'article
        "texte"
        "date": #format 'string' type %d %B %YYYY %H:%M
        "vote": # -1 contre, 0 neutre, 1 pour
    },
    ]
    '''
    arguments = []
    for n in liste.find_all("li"):
        argument = {}
        argument["id"] = n.get("id").replace("arg-", "")
        article = n.find_all("a")[-1]
        argument["article_link"] = URL+unicode((article.get("href")).encode("utf-8"))
        argument["link"] = argument["article_link"]+"#arg-"+argument["id"]
        argument["article_title"] =unicode(clean_text(article.text)).encode("utf-8")
        argument["date"] = format_date(unicode((n.find("p", {"class":"opinion__date"}).text).encode("utf-8")))
        argument["votes"]  = int(n.find("span",{"class":"opinion__votes-nb"}).text)
        arguments.append(argument)
    return arguments

def get_stats(tree):
    '''
    {
        u'modifications': 8, 
        u'votes': 2205, 
        u'sources': 68, 
        u'arguments': 97, 
        u'projets': 0, 
        u'propositions': 2, 
        u'commentaires': 0, 
        u'contributions': 175, #correspondent à toutes les actions sauf les votes
        u'projets participatifs': 0
    }
    '''
    profile_stats = tree.find("div",{"class":"profile__values"})
    #print profile_stats
    stats_title = [clean_text(n.text).lower() for n in profile_stats.find_all("h2")]
    #print stats_title
    stats_values = [clean_text(n.text) for n in profile_stats.find_all("p")]
    user_stats = {k+"_count":int(v) for k,v in zip(stats_title, stats_values) if k not in ['projets participatifs', 'commentaires', 'projets']}
    return user_stats


    
def get_participation(profile_url):
    r = requests.get(profile_url)
    
    if r.status_code == 200:
        tree = BeautifulSoup(r.text)
        #print r.text
        stats = get_stats(tree)
        
        
        doc_index = ["propositions", "modifications","arguments", "sources", "projects", "commentaires", "votes"]
        html_struct = tree.find_all("div", {"class":"container--custom"})[1:]
        
        doc = {k:v for k,v in zip(doc_index, html_struct) if k+"_count" in stats.keys()}
        
        user = {}
        
        for k, v in doc.items():
            #print k
            if k.startswith("contributions"):
                pass
            else:
                obj = k.replace("_count", "")
                func =  "define_"+obj
                
                user[obj] = getattr(sys.modules[__name__], "define_%s" % k)(v)
            for k, v in stats.items():
                user[k] = v
                
            
        return user
    else:
        return False
        
def get_all_participants():
    
    missing = []
    users = []
    with open(udata, "r") as f:
        reader = csv.reader(f, delimiter="\t")
    #udata = read_from_csv(udata)
        for i,row in enumerate(reader):
            if i == 0:
                pass
            else:
                name, url = row
                print url
                uf = os.path.join(u_dir, name+".json")
                if os.path.exists(uf) is False:
                    data = get_participation(url)
                    if data is not False:
                        users.append(data)
                        write_to_json(data, uf)
                    else:
                        missing.append([name, url])
    while len(missing) > 0:
        for n,u in missing:
            participation = get_participation(u)
            if participation is not False:
                uf = os.path.join(u_dir, n+".json")
                data = get_participation(u)
                if data is not False:
                    users.append(data)
                    write_to_json(data, uf)
                    missing.remove([n,u])
                else:
                    pass
def merge_participants():
    import os
    dataset_dir = os.path.join(current_path, "datasets")
    u_dir = os.path.join(dataset_dir, "users")
    participants = []
    for n in os.listdir(u_dir):
        data = read_from_json(os.path.join(u_dir, n))
        participants.append({n.replace(".json", ""):data})
    return participants
if __name__=="__main__":
    data = {"participants":merge_participants()}
    write_to_json(data, parts)
    
    #print get_participation("http://www.republique-numerique.fr/profile/vincentreverdy")
    #get_participations(profile = "/profile/gouvernement")
    #get_gouv_participations()
