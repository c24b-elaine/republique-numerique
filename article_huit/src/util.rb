#!/env/ruby

def array2d_to_file file, array
	CSV.open(file, 'w') do |csv|
		array.each { |e| csv << e }
	end
end

