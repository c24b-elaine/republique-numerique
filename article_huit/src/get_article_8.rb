#!/env/ruby

require 'json'

def get_article
	puts "Loading all articles..."
	f_articles = File.read('../../datasets/articles.json')

	# Load all articles
	puts "Parsing articles to json..."
	articles = JSON.parse(f_articles)['articles']

	puts "Found #{articles.size} articles, keeping article 8"
	articles.select { |a| a['article_id'] == 68 }
end

if __FILE__ == $0
	article_eight = get_article
	File.open('../data/article_huit.json', 'w') { |f| f.puts article_eight.to_json }
end

