#!/env/ruby

require_relative 'util.rb'
require_relative 'get_article_8.rb'
require 'date'
require 'json'
require 'csv'

def get_all_votes
	text = File.read '../../datasets/participants.json'
	JSON.parse(text)['participants']
end

def linked_with link, all_votes = {}
	votes = []
	all_votes = get_all_votes if all_votes.size == 0
	all_votes.each do |vote|
		vote.each_pair do |k, v|
			v['votes'].select { |e| e['link'] == link }.map do |e|
				data = []
				data << k
				data << e['opinion']
				date = DateTime.parse e['date']
				data << date.strftime('%d/%m/%Y')
				data << date.strftime('%H:%M:%S')
				data << date
				votes << data
			end
		end
	end
	votes.sort! { |a, b| a[-1] <=> b[-1] }
	votes.map! { |e| e[0..-2] }
end

if __FILE__ == $0
	text = File.read '../data/article_huit.json'
	art = JSON.parse(text)[0]
	link = art['article_link']
	all_votes = [['voter', 'opinion', 'vote_date', 'vote_time']]
	linked_with(link).each { |v| all_votes << v }
	array2d_to_file '../data/votes.csv', all_votes
end

