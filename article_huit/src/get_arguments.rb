#!/env/ruby

require_relative 'get_votes.rb'

def flatten argument_data
	wrapper2d = []
	argument = []
	argument << argument_data['author']
	argument << argument_data['positive']
	date = DateTime.parse argument_data['created_at']
	argument << date.strftime('%d/%m/%Y')
	argument << date.strftime('%H:%M:%S')
	argument << argument_data['votes_count']
	wrapper2d << argument
	wrapper2d
end

def flatten_with_votes argument_data, votes
	arguments = []
	votes.each do |vote|
		argument = flatten(argument_data).flatten
		vote.each { |v| argument << v }
		arguments << argument
	end
	arguments
end

def is_positive? arg, pos_args
	author = arg['author']
	date = arg['created_at']
	pos_args.find_index { |e| e['author'] == author && e['date'] == date } != nil
end

def get_arguments argument_data, all_votes = {}
	/(?<link>[^#]*#[^#]*)/ =~ argument_data['link']
		votes = linked_with link, all_votes
	votes.size == 0 ? flatten(argument_data) : flatten_with_votes(argument_data, votes)
end

if __FILE__ == $0
	text = File.read '../data/article_huit.json'
	arguments = JSON.parse(text)[0]['arguments']
	text = File.read '../data/positive_arguments.json'
	pos_args = JSON.parse(text)
	all_votes = get_all_votes
	all_args = [['author', 'positive', 'argument_date', 'argument_time', 'total_votes', 'voter', 'opinion', 'vote_date', 'vote_time']]
	arguments.sort! { |a, b| a['created_at'] <=> b['created_at'] }
	arguments.each do |arg|
		arg['positive'] = is_positive? arg, pos_args
		get_arguments(arg, all_votes).each { |a| all_args << a }
	end
	array2d_to_file '../data/arguments.csv', all_args
end

