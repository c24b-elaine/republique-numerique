#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os
import sys
import csv
import pprint

with open('./datasets/votes_art_8.csv', 'rb') as csv_file:
	jours = {}
	csv_reader = csv.DictReader(csv_file, delimiter = ",", quotechar = '"')
	for row in csv_reader:
		if(row["date"] in jours):
			jours[row["date"]]["votes"] += 1
			if row["vote"] == str(1):
				jours[row["date"]]["positive"] += 1
			elif row["vote"] == str(-1):
				jours[row["date"]]["negative"] += 1
			elif row["vote"] == str(0):
				jours[row["date"]]["mixed"] += 1
		else:
			jours[row["date"]] = {"votes" : 0 , "positive" : 0, "negative" : 0, "mixed" : 0}
	
with open("./datasets/votes_art_8_sorted_by_day.csv", "wb") as csv_file_2:
	fieldnames = ['date', 'votes', 'positive', 'negative', 'mixed']
	writer = csv.DictWriter(csv_file_2, fieldnames=fieldnames)
	writer.writeheader()
	
	for key, row in jours.iteritems():
		writer.writerow({"date":key, "votes":row["votes"], "positive":row["positive"], "negative":row["negative"], "mixed":row["mixed"]})
		
			
