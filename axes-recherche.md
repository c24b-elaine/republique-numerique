# Projet de loi république numérique


## Présentation du projet de loi
*****************

* [Le texte de loi intégral initial] (https://www.republique-numerique.fr/pages/projet-de-loi-pour-une-republique-numerique)

qui présente le projet loi tel qu'il a été rédigé au départ

* [La consultation en ligne] (https://www.republique-numerique.fr/consultation/consultation):

qui présente: 
    * les articles (article de loi proposés ou ajoutés)
    * les propositions (modification des articles)
    * les votes (pour contre mitigé)
    * les arguments (en faveur ou contre un article et en justification d'un vote)

    
* [les réponses du gouvernement ](https://www.republique-numerique.fr/project/projet-de-loi-numerique/ranking/reponses-du-gouvernement)

Les réponses du gouvernement sont attendues fin novembre 
Pour le moment:
        * classement des articles les plus votés
        * classement des propositions les plus votés

:warning: une erreur s'est glissée dans le site,

le lien pour afficher tous les articles à plat n'est pas disponible

mais seulement les '''propositions'''
  
    
* [la synthèse proposée par ISLean Consulting via l'outil de CapCollectif] (https://www.republique-numerique.fr/project/projet-de-loi-numerique/synthesis/synthese-1)

Une bonne note méthodologie pour appréhender les différents datasets et la nomenclature



## Présentation des datasets
*********************

Tous les datasets  sont disponibles aux téléchargements depuis cette plateforme 
et chaque dataset disponible est documenté dans le [wiki] (../wikis/datasets)
Vous y trouverez la nomenclature des données, 
une rapide explication de leur contexte d'extraction
et quelques pistes d'analyses et quelques pistes de méthodes

### Les [articles] (../wikis/articles) soumis au votes :

Les articles proposés pour le vote disponible sur [le site] (https://www.republique-numerique.fr/pages/projet-de-loi-pour-une-republique-numerique)

* :open_file_folder: [CSV] (../blob/master/proposition.csv) 
    
    avec les champs suivants:
        ```article_subtitle, titre_subtitle,section_subtitle,titre_nb,chapter_nb,article_text,section_nb,article_nb,chapter_subtitle```
    
* :open_file_folder: [JSON] (../blob/master/proposition.json) 
    
    avec les champs suivants:
        ```
        {nb_article:
                {"article_nb":art[0], 
                "article_subtitle": art[1], 
                "titre_nb":titr[0], 
                "titre_subtitle": titr[1],
                "chapter_nb":chap[0], 
                "chapter_subtitle": chap[1], 
                "section_nb":sect[0], 
                "section_subtitle": sect[1],
                "article_text": article}        
        ```
L'unité minimale est toujours l'article de loi

Le code qui a permis sa génération est disponible [ici] (../blob/master/scripts/extraction/article_loi.py)

### Les ''votes''

Nombre de votes pour, nombre de votes contre, pour, id_article, créé par

### Les contributeurs

### Les modifications

### Les arguments

### L'historique des participations

### Les synthèse proposées


## Questions de recherche
*********************************

## Equipes
*********************************

## Méthodes et analyses
**********************************

## Visulaisations
***********************************

## TO DO:

1. encode properly [propositions du gouvernement+ citoyens](../blob/master/arguments_all.json)
2. encode properly [propositions du gouvernement](../blob/master/arguments_gouvernement.json)
3. gérer les versions historicisées
4. extraire la proposition présentée en conseil des ministres



## Proposition d'axes d'études
* Analyse du contenu du projet de loi (VO):
    * Comment caractériser le texte du projet de loi?
    * Emergence de thématiques  mots-clés, sujets, polarités
    * Croisement entre méthode de texmining et travaux préalables
    * Structure interne de la consultation
    * Catégorisation par le site, Catégorisation par la structure du site, Catégorisation par les travaux préalables
    * Référence internes au projet de loi et externe (autres codes, modifications des lois existantes)


* Analyse de versions (VO, VF, VF2):
    * Comment caractériser les modifications faites au projet de loi initial?
    * Nombre d’articles ajoutés à la VO
    * Nombre d’articles ajoutés qui n’émanent pas du gouvernement
    * Par quels acteurs?
    * Quels arguments retenus? Sur quels critères ? 
    * Par quels acteurs ?
* Version historique: 
    * evolution du débat
    * evolution de la participation

* Analyse du débat
    * Analyse de la teneur du débat et de sa vivacité
    * Comment caractériser la teneur du débat en ligne
    * Top des articles les plus votés
    * Top des articles les plus modifiés
    * Top des articles les plus polarisés (+ Quelle mesure de controverse)
    * Caractérisation des sujets
* Analyse des parties prenantes /acteurs
    * Comment caractériser la participation des acteurs sur le projet de loi
    * Contributeur les plus actifs
    * Contributeurs les plus polarisés
    * Contributeurs spécifiques ou spécialisés
    * Catégorisation des acteurs : quelle typologie?

* Analyse des synthèses 

